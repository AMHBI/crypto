import React, { useEffect, useState } from "react";

//API:
import { getCoins } from "../Services/api";

//Component:
import Coin from "./Coin";
import Loader from "./Loader";
//Styles:
import styles from "../Styles/Main.module.css";
const Main = () => {
  const [coins, setCoins] = useState([]);
  const [serach, setSerach] = useState("");
  useEffect(() => {
    const fetchAPI = async () => {
      const data = await getCoins();
      setCoins(data);
    };
    fetchAPI();
  }, []);

  const searchHandler = (event) => {
    setSerach(event.target.value);
  };
  const searchedCoins = coins.filter((c) =>
    c.name.toLowerCase().includes(serach.toLowerCase())
  );
  return (
    <>
      <input
        type='text'
        placeholder='Search...'
        value={serach}
        onChange={searchHandler}
        className={styles.input}
      />
      <div className={styles.coinContainer}>
        <div className={styles.labelContainer}>
            <span>name</span>
            <span>symbol</span>
            <span>current price</span>
            <span>24h change</span>
            <span>market cap</span>
        </div>
        {coins.length ? (
          searchedCoins.map((c) => {
            return (
              <Coin
                key={c.id}
                image={c.image}
                symbol={c.symbol}
                name={c.name}
                price={c.current_price}
                priceChange={c.market_cap_change_percentage_24h}
                marketCap={c.market_cap}
              />
            );
          })
        ) : (
          <Loader />
        )}
      </div>
    </>
  );
};

export default Main;
